package ru.vartanyan.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;

@Component
public class ExitListener extends AbstractListener {

    @Override
    public String arg() {
        return "-e";
    }

    @Override
    public @NotNull String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit";
    }

    @Override
    @EventListener(condition = "@exitListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.exit(0);
    }

}
