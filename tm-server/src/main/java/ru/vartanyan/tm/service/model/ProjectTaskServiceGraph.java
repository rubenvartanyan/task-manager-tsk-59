package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IProjectTaskServiceGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.repository.model.ProjectRepositoryGraph;
import ru.vartanyan.tm.repository.model.TaskRepositoryGraph;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class ProjectTaskServiceGraph implements IProjectTaskServiceGraph {

    @NotNull
    @Autowired
    public ITaskRepositoryGraph taskRepositoryGraph;

    @NotNull
    @Autowired
    public IProjectRepositoryGraph projectRepositoryGraph;

    @NotNull
    public IProjectRepositoryGraph getProjectRepositoryGraph() {
        return projectRepositoryGraph;
    }

    @NotNull
    public ITaskRepositoryGraph getTaskRepositoryGraph() {
        return taskRepositoryGraph;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.bindTaskByProjectId(userId, projectId, taskId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @NotNull List<TaskGraph> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        return taskRepositoryGraph.findAllByProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        IProjectRepositoryGraph projectRepositoryGraph = getProjectRepositoryGraph();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.removeAllByProjectId(userId, projectId);
        projectRepositoryGraph.removeOneByIdAndUserId(userId, projectId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        taskRepositoryGraph.unbindTaskFromProjectId(userId, taskId);
    }

}
