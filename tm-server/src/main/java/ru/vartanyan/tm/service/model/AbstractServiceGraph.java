package ru.vartanyan.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.dto.IProjectRepository;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.model.IServiceGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.model.AbstractEntityGraph;

import javax.persistence.EntityManager;

@NoArgsConstructor
@Service
public abstract class AbstractServiceGraph<E extends AbstractEntityGraph> implements IServiceGraph<E> {

}
