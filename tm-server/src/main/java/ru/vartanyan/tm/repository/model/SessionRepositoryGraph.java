package ru.vartanyan.tm.repository.model;


import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.model.SessionGraph;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public class SessionRepositoryGraph extends AbstractRepository<SessionGraph> implements ISessionRepositoryGraph {


    @NotNull
    public List<SessionGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionGraph e", SessionGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    public SessionGraph findOneById(@Nullable final String id) {
        return entityManager.find(SessionGraph.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionGraph e")
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    public void removeOneById(@Nullable final String id) {
        SessionGraph reference = entityManager.getReference(SessionGraph.class, id);
        entityManager.remove(reference);
    }

}