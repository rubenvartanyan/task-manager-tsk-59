package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.model.*;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.service.TestUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class UserServiceGraphTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectServiceGraph projectService =
            context.getBean(IProjectServiceGraph.class);

    @NotNull
    private final IUserServiceGraph userService =
            context.getBean(IUserServiceGraph.class);

    @NotNull
    private final ITaskServiceGraph taskService =
            context.getBean(ITaskServiceGraph.class);

    {
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<UserGraph> users = new ArrayList<>();
        final UserGraph user1 = new UserGraph();
        final UserGraph user2 = new UserGraph();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertTrue(userService.findOneById(user1.getId()) != null);
        Assert.assertTrue(userService.findOneById(user2.getId()) != null);
        userService.remove(users.get(0));
        userService.remove(users.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() throws NullObjectException {
        final UserGraph user = new UserGraph();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int userSize = userService.findAll().size();
        userService.create("testFindAll", "test", "-");
        Assert.assertEquals(userSize + 1, userService.findAll().size());
        userService.removeByLogin("testFindAll");
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() throws NullObjectException {
        final UserGraph user = new UserGraph();
        user.setLogin("testFindL");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.findByLogin(login) != null);
        userService.removeByLogin("testFindL");
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() throws NullObjectException {
        final UserGraph user = new UserGraph();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() throws NullObjectException {
        final UserGraph user = new UserGraph();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.findOneById(userId) != null);
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() throws NullObjectException {
        final UserGraph user = new UserGraph();
        user.setLogin("testRemoveByLogin");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.removeByLogin(login);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() throws NullObjectException {
        final UserGraph user = new UserGraph();
        userService.add(user);
        final String userId = user.getId();
        userService.removeOneById(userId);
        Assert.assertTrue(userService.findOneById(userId) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() throws NullObjectException {
        final UserGraph user = new UserGraph();
        userService.add(user);
        userService.remove(user);
        Assert.assertNull(userService.findOneById(user.getId()));
    }

}
